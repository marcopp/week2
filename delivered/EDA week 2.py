#!/usr/bin/env python
# coding: utf-8

# **EDA**

# In[12]:


import pandas as pd 
import missingno as msn


# In[184]:


cab_data = pd.read_csv("Cab_Data.csv")
city = pd.read_csv("City.csv")
customer_id = pd.read_csv("Customer_ID.csv")
transactions_id = pd.read_csv("transaction_id.csv")


# In[34]:


cab_data.dtypes


# In[35]:


city.dtypes


# In[36]:


customer_id.dtypes


# In[37]:


transactions_id.dtypes


# **Relations  tables** <br> 
# cab_data <- (Transactions, key: Transaction ID) <br>
# cab_data <- (city, key: City) <br>
# transactions <- (customer_id, key: Customer ID)
#            

# **Feature Transformations**

# **cab date**

# In[65]:


cab_data["date"]=pd.to_datetime(pd.to_numeric(cab_data["Date of Travel"],errors='coerce'),errors='coerce',origin='1899-12-30',unit='D')
cab_data.drop("Date of Travel", axis=1, inplace=True)


# In[85]:


cab_data.head()


# **City population**

# In[ ]:


city["Population"] = city["Population"].str.replace(",","")
city["Population"] = city["Population"].astype(int)


# In[82]:


city["Users"] = city["Users"].str.replace(",","")
city["Users"] = city["Users"].astype(int)


# In[83]:


city.head()


# **Transactions**

# In[98]:


transactions_id["Payment_Mode"] = pd.get_dummies(transactions_id["Payment_Mode"], drop_first = True)


# In[100]:


transactions_id.head()


# **Describe**

# In[108]:


cab_data.describe()


# In[112]:


customer_id.describe()


# In[115]:


transactions_id.describe()


# **Master data**

# In[126]:


Master_data = cab_data.merge(city, on="City", how="left")
Master_data = Master_data.merge(transactions_id, on="Transaction ID", how="left")
Master_data = Master_data.merge(customer_id, on="Customer ID", how="left")


# In[128]:


Master_data.head()


# **Transformations Master data**

# In[130]:


Master_data["prop_user"] = Master_data["Users"] / Master_data["Population"]
Master_data.drop(["Population", "Users"], axis=1, inplace=True)


# In[131]:


Master_data


# **Investigation and Hypothesis**

# **Revenue**

# In[158]:


Master_data[Master_data.Company == "Pink Cab"].resample("M", on="date")["Price Charged"].sum().plot(legend=True, label="Pink");
Master_data[Master_data.Company == "Yellow Cab"].resample("M", on="date")["Price Charged"].sum().plot(legend=True, label="Yellow");


# **Count Users**

# In[157]:


Master_data[Master_data.Company == "Pink Cab"].resample("M", on="date")["Customer ID"].count().plot(legend=True, label="Pink");
Master_data[Master_data.Company == "Yellow Cab"].resample("M", on="date")["Customer ID"].count().plot(legend=True, label="Yellow");


# **Seasonality**

# In[137]:


Master_data.resample("D", on="date")["Transaction ID"].count().plot();


# In[138]:


Master_data.resample("M", on="date")["Transaction ID"].count().plot();


# In[160]:


Master_data.groupby("Gender")["Age"].count()


# In[166]:


Master_data.Age.plot(kind="kde");


# **Hypothesis**

# In[175]:


Master_data.groupby("City")["prop_user"].first().sort_values(ascending = False) * 100


#  Boston and Washington are the cities with more proportion of users. 

# In[180]:


Master_data.groupby("City")["KM Travelled"].mean().sort_values(ascending = False)


# In[181]:


Master_data


# In average a trip in a cab consists in 22 km without matter the city 

# In[183]:


Master_data["Payment_Mode"].value_counts().plot(kind="bar");


# There is a slight preference for pay with Credit card

# In[186]:


Master_data.Age.plot(kind="kde");


# The average customer has 20-40 years

# In[189]:


Master_data[Master_data.Company == "Pink Cab"]["Income (USD/Month)"].plot(kind="kde", legend=True, label="Pink");
Master_data[Master_data.Company == "Yellow Cab"]["Income (USD/Month)"].plot(kind="kde", legend=True, label="Yellow")


# There's no preference for one or another company for customers accoridng to his income
